﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zopa
{
    public static class HelperMethods
    {
        public static void WriteToLogFile(string log)
        {
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "/log.txt", log);
        }

        public static void ScrollToElement(By locator)
        {
            var element = Browser.Driver.FindElement(locator);
            ScrollToElement(element);
        }

        public static void ScrollToElement(IWebElement element)
        {
            var actions = new Actions(Browser.Driver);
            actions.MoveToElement(element);
            actions.Perform();
        }
    }
}
