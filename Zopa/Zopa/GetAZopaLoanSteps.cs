﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Zopa.Pages;

namespace Zopa
{
    [Binding]
    public sealed class GetAZopaLoanSteps
    {
        private HomePage homePage = new HomePage();
        private LoansPage loansPage = new LoansPage();
        private CalculateRatesPage calculateRatesPage = new CalculateRatesPage();

        [Given(@"I am on the zopa home page")]
        public void GivenIAmOnTheZopaHomePage()
        {
            Browser.Driver.Url = "https://www.zopa.com";
            homePage.CloseCookieFooter();
        }

        [When(@"I apply for a loan with valid details")]
        public void WhenIApplyForALoanWithValidDetails()
        {
            homePage.ClickGetLoan();
            loansPage.ClickGetRates();
            calculateRatesPage.RegisterRandomUser();
        }

        [Then(@"placeholder step")]
        public void ThenPlaceholderStep()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
