﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zopa.Pages
{
    public class LoansPage
    {
        public By GetMyPersonalisedRatesButton = By.Id("submit-loan-button");

        public void ClickGetRates()
        {
            HelperMethods.ScrollToElement(GetMyPersonalisedRatesButton);
            Browser.Driver.FindElement(GetMyPersonalisedRatesButton).Click();
        }
    }
}
