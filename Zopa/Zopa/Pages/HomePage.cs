﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zopa.Pages
{
    public class HomePage
    {
        private By GetAZopaLoanButton = By.XPath("//a[.='Get a Zopa loan']");

        private By CloseCookiesMessageButton = By.XPath("//*[@id='footer']//*[@class='close']");

        public void ClickGetLoan()
        {
            Browser.Driver.FindElements(GetAZopaLoanButton)[1].Click();
        }

        public void CloseCookieFooter()
        {
            Browser.Driver.FindElement(CloseCookiesMessageButton).Click();
        }
    }
}
