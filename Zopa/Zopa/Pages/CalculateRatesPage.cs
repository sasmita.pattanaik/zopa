﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zopa.Pages
{
    public class CalculateRatesPage
    {
        private By EmailAddress = By.Id("member_email");

        private By TitleCheckboxes = By.Name("applications_loan_apply[title]");

        private By FirstName = By.Id("applications_loan_apply_first_name");
        private By LastName = By.Id("applications_loan_apply_last_name");
        private By PhoneNumber = By.Id("applications_loan_apply_home_phone");

        private By DateOfBirthDay = By.Id("date_of_birth_day");
        private By DateOfBirthMonth = By.Id("date_of_birth_month");
        private By DateOfBirthYear = By.Id("date_of_birth_year");

        private By ShowMoreLoanReasonsButton = By.XPath(".//a[@href='#additional-loan-purposes']");
        private By LoanReasonCheckboxes = By.Name("applications_loan_apply[loan_purpose]");

        private By Postcode = By.Id("address_postcode");
        private By LookUpAddress = By.Name("find_address");

        private By EmploymentStatusCheckboxes = By.Name("applications_loan_apply[employment_status]");

        private By AnnualIncomeBeforeTax = By.Id("applications_loan_apply_salary");

        private By DoYouOwnYourHomeCheckboxes = By.Name("applications_loan_apply[residential_status]");

        private By MonthlyRent = By.Id("applications_loan_apply_rent");

        private By Password = By.Id("member_password");

        public void RegisterRandomUser()
        {
            var logMessage = new StringBuilder("Registered User: ");

            string email = Faker.InternetFaker.Email();
            Browser.Driver.FindElement(EmailAddress).SendKeys(email);
            logMessage.Append("\n" + email);

            string title = ClickRandomCheckbox(TitleCheckboxes);
            string firstName = Faker.NameFaker.FirstName();
            Browser.Driver.FindElement(FirstName).SendKeys(firstName);
            string lastName = Faker.NameFaker.LastName();
            Browser.Driver.FindElement(LastName).SendKeys(lastName);
            logMessage.Append("\n" + title + " " + firstName + " " + lastName);

            string phoneNumber = Faker.PhoneFaker.Phone();
            Browser.Driver.FindElement(PhoneNumber).SendKeys(phoneNumber);
            logMessage.Append("\n" + phoneNumber);

            DateTime dateOfBirth = Faker.DateTimeFaker.BirthDay();
            Browser.Driver.FindElement(DateOfBirthDay).SendKeys(dateOfBirth.Day.ToString());
            Browser.Driver.FindElement(DateOfBirthMonth).SendKeys(dateOfBirth.Month.ToString());
            Browser.Driver.FindElement(DateOfBirthYear).SendKeys(dateOfBirth.Year.ToString());
            logMessage.Append("\n" + dateOfBirth.ToString("dd/MM/yyyy"));

            HelperMethods.ScrollToElement(ShowMoreLoanReasonsButton);
            Browser.Driver.FindElement(ShowMoreLoanReasonsButton).Click();
            string loanReason = ClickRandomCheckbox(LoanReasonCheckboxes);
            logMessage.Append("\n" + loanReason);

            string postCode = Faker.LocationFaker.PostCode();
            Browser.Driver.FindElement(Postcode).SendKeys(postCode);
            logMessage.Append("\n" + postCode);

            string employmentDetails = ClickRandomCheckbox(EmploymentStatusCheckboxes);
            logMessage.Append("\n" + employmentDetails);

            int annualIncome = Faker.NumberFaker.Number(0, 100000);
            Browser.Driver.FindElement(AnnualIncomeBeforeTax).SendKeys(annualIncome.ToString());
            logMessage.Append("\n" + annualIncome);

            string homeOwner = ClickRandomCheckbox(DoYouOwnYourHomeCheckboxes);
            logMessage.Append("\n" + homeOwner);

            HelperMethods.ScrollToElement(Password);
            string password = Faker.StringFaker.AlphaNumeric(8) + "Aa1"; // to pass password requirements
            Browser.Driver.FindElement(Password).SendKeys(password);
            logMessage.Append("\n" + password);

            HelperMethods.WriteToLogFile(logMessage.ToString());
        }

        private string ClickRandomCheckbox(By checkboxesLocator)
        {
            var checkboxes = Browser.Driver.FindElements(checkboxesLocator);
            int randomNumber = Faker.NumberFaker.Number(0, checkboxes.Count - 1);
            HelperMethods.ScrollToElement(checkboxes[randomNumber]);
            checkboxes[randomNumber].FindElement(By.XPath("./following-sibling::label")).Click();
            return checkboxes[randomNumber].GetAttribute("value");
        }
    }
}
